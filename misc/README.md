## Key generator

There is a key generator in this directory. The source code can be found at 
https://github.com/yggdrasil-network/yggdrasil-go/blob/master/contrib/ansible/genkeys.go,
it is licensed under the GNU LGPLv3.